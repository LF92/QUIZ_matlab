function varargout = projekt(varargin)
% PROJEKT MATLAB code for projekt.fig
%      PROJEKT, by itself, creates a new PROJEKT or raises the existing
%      singleton*.
%
%      H = PROJEKT returns the handle to a new PROJEKT or the handle to
%      the existing singleton*.
%
%      PROJEKT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PROJEKT.M with the given input arguments.
%
%      PROJEKT('Property','Value',...) creates a new PROJEKT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before projekt_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to projekt_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help projekt

% Last Modified by GUIDE v2.5 21-Jan-2015 14:48:36

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @projekt_OpeningFcn, ...
                   'gui_OutputFcn',  @projekt_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before projekt is made visible.
function projekt_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to projekt (see VARARGIN)

% Choose default command line output for projekt
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes projekt wait for user response (see UIRESUME)
% uiwait(handles.figure1);

global PUNKTY;
global PYTANIE;
global NRPYTANIA;
global LICZNIK;
global BAZAPYTAN;
global FS;
global BAD;
global GOOD;
global DOBREODP;
global ZLEODP;
global ODPOWIEDZI;
global MAX;
global ODP1 ODP2 ODP3 ODP4 ODP5 ODP6 ODP7 ODP8 ODP9 ODP10;

MAX = 10;
ODPOWIEDZI = zeros(MAX);
DOBREODP = 0;
ZLEODP = 0;
PUNKTY = 0;

NRPYTANIA = 1;
FS = 12000;


BAD = wavread('bad.wav');
GOOD = wavread('good.wav');
%BAD = audioplayer(bad,FS);
%GOOD = audioplayer(good,FS);

BAZAPYTAN = {
    'Kt�rego protoko�u nale�y u�y� do odbioru poczty elektronicznej ze swojego serwera?'
    'Kt�ry protok� zapewnia szyfrowanie po��czenia?'
    'Pa�stwo z kt�rym nie s�siaduje Polska to'
    'W kt�rym roku by�a ostatnia misja za�ogi Apollo?'
    '1 bajt to:'
    'Kt�ra z podanych sieci jest najwi�ksza?'
    'Rzesz�w to stolica wojew�dztwa:'
    'Satelit� Marsa jest '
    '��w jest:'
    'Kopenhaga jest stolic�:'};

ODP1 = {'POP3','SNMP','FTP'}; %1
ODP2 = {'DNS','DHCP','SSH'}; %3
ODP3 = {'S�owacja','Litwa','W�gry'}; %3
ODP4 = {'1976 ','1972 ','1974'}; %2
ODP5 = {'8 bit�w','4 bity','32 bity'}; %1
ODP6 = {'Lan','Wan','Man'}; %2
ODP7 = {'wielkopolskiego','ma�opolskiego','podkarpackiego'}; %3
ODP8 = {'Ariel','Deimos','Lizytea'}; %2
ODP9 = {'gadem','ptakiem','p�azem'}; %1
ODP10 = {'Oslo','Danii','Sztokholmu'}; %2


PYTANIE = BAZAPYTAN{NRPYTANIA};

set(handles.text3,'String',PUNKTY);

set(handles.text1,'String',PYTANIE);
set(handles.odppierwsza,'String',ODP1{1});
set(handles.odpdruga,'String',ODP1{2});
set(handles.odptrzecia,'String',ODP1{3});

LICZNIK = strcat(num2str(NRPYTANIA), ' / ', num2str(MAX));
set(handles.text5,'String',LICZNIK);

    set(handles.text1,'visible','off');
    set(handles.text2,'visible','off');
    set(handles.text4,'visible','off');
    set(handles.text3,'visible','off');
    set(handles.text5,'visible','off');
    set(handles.odppierwsza,'visible','off');
    set(handles.odpdruga,'visible','off');
    set(handles.odptrzecia,'visible','off');
    set(handles.uipanel1,'visible','off');
    set(handles.wroc,'visible','off');
    set(handles.infoomnie,'visible','off');
    
    set(handles.wyniki,'visible','off');
    set(handles.podsumowanie,'visible','off');
    set(handles.koniec2,'visible','off');


% --- Outputs from this function are returned to the command line.
function varargout = projekt_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

function nowagra_Callback(hObject, eventdata, handles)


set(handles.nowagra,'visible','off');
set(handles.oautorze,'visible','off');
set(handles.koniec,'visible','off');


set(handles.text1,'visible','on');
set(handles.text2,'visible','on');
set(handles.text4,'visible','on');
set(handles.text3,'visible','on');
set(handles.text5,'visible','on');
set(handles.odppierwsza,'visible','on');
set(handles.odpdruga,'visible','on');
set(handles.odptrzecia,'visible','on');
set(handles.uipanel1,'visible','on');




function odppierwsza_Callback(hObject, eventdata, handles)
global MAX;
global PUNKTY;
global GOOD;
global BAD;
global FS;
global PYTANIE;
global NRPYTANIA;
global BAZAPYTAN;
global DOBREODP;
global ZLEODP;
global LICZNIK;
global ODPOWIEDZI;
global ODP2 ODP3 ODP4 ODP5 ODP6 ODP7 ODP8 ODP9 ODP10;


if NRPYTANIA == 1
    msgbox({'Odpowied� prawid�owa' '' 'Dostajesz 1 punkt'});
    wavplay(GOOD,FS);
    PUNKTY = PUNKTY + 1;
    DOBREODP = DOBREODP + 1;
    ZLEODP = ZLEODP + 0;
    ODPOWIEDZI(NRPYTANIA) = 1;
    set(handles.text3,'String',PUNKTY);
    NRPYTANIA = NRPYTANIA + 1;
    PYTANIE = BAZAPYTAN{NRPYTANIA};
    set(handles.text1,'String',PYTANIE);
    set(handles.odppierwsza,'String',ODP2{1});
    set(handles.odpdruga,'String',ODP2{2});
    set(handles.odptrzecia,'String',ODP2{3});
    LICZNIK = strcat(num2str(NRPYTANIA), ' / ', num2str(MAX));
    set(handles.text5,'String',LICZNIK);
    
elseif NRPYTANIA == 2
    msgbox({'Odpowied� nieprawid�owa' '' 'Dostajesz 0 punkt�w'});
    wavplay(BAD,FS);
    PUNKTY = PUNKTY + 0;
    DOBREODP = DOBREODP + 0;
    ZLEODP = ZLEODP + 1;
    ODPOWIEDZI(NRPYTANIA) = 0;
    set(handles.text3,'String',PUNKTY);
    NRPYTANIA = NRPYTANIA + 1;
    PYTANIE = BAZAPYTAN{NRPYTANIA};
    set(handles.text1,'String',PYTANIE);
    set(handles.odppierwsza,'String',ODP3{1});
    set(handles.odpdruga,'String',ODP3{2});
    set(handles.odptrzecia,'String',ODP3{3});
    LICZNIK = strcat(num2str(NRPYTANIA), ' / ', num2str(MAX));
    set(handles.text5,'String',LICZNIK);
    
elseif NRPYTANIA == 3
    msgbox({'Odpowied� nieprawid�owa' '' 'Dostajesz 0 punkt�w'});
    wavplay(BAD,FS);
    PUNKTY = PUNKTY + 0;
    DOBREODP = DOBREODP + 0;
    ZLEODP = ZLEODP + 1;
    ODPOWIEDZI(NRPYTANIA) = 0;
    set(handles.text3,'String',PUNKTY);
    NRPYTANIA = NRPYTANIA + 1;
    PYTANIE = BAZAPYTAN{NRPYTANIA};
    set(handles.text1,'String',PYTANIE);
    set(handles.odppierwsza,'String',ODP4{1});
    set(handles.odpdruga,'String',ODP4{2});
    set(handles.odptrzecia,'String',ODP4{3});
    LICZNIK = strcat(num2str(NRPYTANIA), ' / ', num2str(MAX));
    set(handles.text5,'String',LICZNIK);
    
elseif NRPYTANIA == 4
    msgbox({'Odpowied� nieprawid�owa' '' 'Dostajesz 0 punkt�w'});
    wavplay(BAD,FS);
    PUNKTY = PUNKTY + 0;
    DOBREODP = DOBREODP + 0;
    ZLEODP = ZLEODP + 1;
    ODPOWIEDZI(NRPYTANIA) = 0;
    set(handles.text3,'String',PUNKTY);
    NRPYTANIA = NRPYTANIA + 1;
    PYTANIE = BAZAPYTAN{NRPYTANIA};
    set(handles.text1,'String',PYTANIE);
    set(handles.odppierwsza,'String',ODP5{1});
    set(handles.odpdruga,'String',ODP5{2});
    set(handles.odptrzecia,'String',ODP5{3});
    LICZNIK = strcat(num2str(NRPYTANIA), ' / ', num2str(MAX));
    set(handles.text5,'String',LICZNIK);
    
elseif NRPYTANIA == 5
    msgbox({'Odpowied� prawid�owa' '' 'Dostajesz 1 punkt'});
    wavplay(GOOD,FS);
    PUNKTY = PUNKTY + 1;
    DOBREODP = DOBREODP + 1;
    ZLEODP = ZLEODP + 0;
    ODPOWIEDZI(NRPYTANIA) = 1;
    set(handles.text3,'String',PUNKTY);
    NRPYTANIA = NRPYTANIA + 1;
    PYTANIE = BAZAPYTAN{NRPYTANIA};
    set(handles.text1,'String',PYTANIE);
    set(handles.odppierwsza,'String',ODP6{1});
    set(handles.odpdruga,'String',ODP6{2});
    set(handles.odptrzecia,'String',ODP6{3});
    LICZNIK = strcat(num2str(NRPYTANIA), ' / ', num2str(MAX));
    set(handles.text5,'String',LICZNIK);
    
elseif NRPYTANIA == 6
    msgbox({'Odpowied� nieprawid�owa' '' 'Dostajesz 0 punkt�w'});
    wavplay(BAD,FS);
    PUNKTY = PUNKTY + 0;
    DOBREODP = DOBREODP + 0;
    ZLEODP = ZLEODP + 1;
    ODPOWIEDZI(NRPYTANIA) = 0;
    set(handles.text3,'String',PUNKTY);
    NRPYTANIA = NRPYTANIA + 1;
    PYTANIE = BAZAPYTAN{NRPYTANIA};
    set(handles.text1,'String',PYTANIE);
    set(handles.odppierwsza,'String',ODP7{1});
    set(handles.odpdruga,'String',ODP7{2});
    set(handles.odptrzecia,'String',ODP7{3});
    LICZNIK = strcat(num2str(NRPYTANIA), ' / ', num2str(MAX));
    set(handles.text5,'String',LICZNIK);
    
elseif NRPYTANIA == 7
    msgbox({'Odpowied� nieprawid�owa' '' 'Dostajesz 0 punkt�w'});
    wavplay(BAD,FS);
    PUNKTY = PUNKTY + 0;
    DOBREODP = DOBREODP + 0;
    ZLEODP = ZLEODP + 1;
    ODPOWIEDZI(NRPYTANIA) = 0;
    set(handles.text3,'String',PUNKTY);
    NRPYTANIA = NRPYTANIA + 1;
    PYTANIE = BAZAPYTAN{NRPYTANIA};
    set(handles.text1,'String',PYTANIE);
    set(handles.odppierwsza,'String',ODP8{1});
    set(handles.odpdruga,'String',ODP8{2});
    set(handles.odptrzecia,'String',ODP8{3});
    LICZNIK = strcat(num2str(NRPYTANIA), ' / ', num2str(MAX));
    set(handles.text5,'String',LICZNIK);
    
elseif NRPYTANIA == 8
    msgbox({'Odpowied� nieprawid�owa' '' 'Dostajesz 0 punkt�w'});
    wavplay(BAD,FS);
    PUNKTY = PUNKTY + 0;
    DOBREODP = DOBREODP + 0;
    ZLEODP = ZLEODP + 1;
    ODPOWIEDZI(NRPYTANIA) = 0;
    set(handles.text3,'String',PUNKTY);
    NRPYTANIA = NRPYTANIA + 1;
    PYTANIE = BAZAPYTAN{NRPYTANIA};
    set(handles.text1,'String',PYTANIE);
    set(handles.odppierwsza,'String',ODP9{1});
    set(handles.odpdruga,'String',ODP9{2});
    set(handles.odptrzecia,'String',ODP9{3});
    LICZNIK = strcat(num2str(NRPYTANIA), ' / ', num2str(MAX));
    set(handles.text5,'String',LICZNIK);
    
elseif NRPYTANIA == 9
    msgbox({'Odpowied� prawid�owa' '' 'Dostajesz 1 punkt'});
    wavplay(GOOD,FS);
    PUNKTY = PUNKTY + 1;
    DOBREODP = DOBREODP + 1;
    ZLEODP = ZLEODP + 0;
    ODPOWIEDZI(NRPYTANIA) = 1;
    set(handles.text3,'String',PUNKTY);
    NRPYTANIA = NRPYTANIA + 1;
    PYTANIE = BAZAPYTAN{NRPYTANIA};
    set(handles.text1,'String',PYTANIE);
    set(handles.odppierwsza,'String',ODP10{1});
    set(handles.odpdruga,'String',ODP10{2});
    set(handles.odptrzecia,'String',ODP10{3});
    LICZNIK = strcat(num2str(NRPYTANIA), ' / ', num2str(MAX));
    set(handles.text5,'String',LICZNIK);
    
elseif NRPYTANIA == 10
    msgbox({'Odpowied� nieprawid�owa' '' 'Dostajesz 0 punkt�w'});
    wavplay(BAD,FS);
    PUNKTY = PUNKTY + 0;
    DOBREODP = DOBREODP + 0;
    ZLEODP = ZLEODP + 1;
    ODPOWIEDZI(NRPYTANIA) = 0;
    set(handles.text3,'String',PUNKTY);

    set(handles.text1,'visible','off');
    set(handles.text2,'visible','off');
    set(handles.text4,'visible','off');
    set(handles.text3,'visible','off');
    set(handles.text5,'visible','off');
    set(handles.odppierwsza,'visible','off');
    set(handles.odpdruga,'visible','off');
    set(handles.odptrzecia,'visible','off');
    set(handles.uipanel1,'visible','off');
    
    
    axes(handles.wyniki);
    bar(ODPOWIEDZI,'stacked','g');
    
    prawidlowe = strcat('Prawid�owe odpowiedzi:  ', num2str(DOBREODP));
    nieprawidlowe = strcat('Nieprawid�owe odpowiedzi:  ', num2str(ZLEODP));
    procent = ((DOBREODP * 100) / MAX);
    procentprawidlowych = strcat('% prawid�owych:  ', num2str(procent), '%');
    set(handles.podsumowanie,'String',{prawidlowe, nieprawidlowe, procentprawidlowych, '','','', 'Pytania na kt�re odpowiedziano poprawnie'});
    set(handles.wyniki,'visible','on');
    set(handles.podsumowanie,'visible','on');
    
    
    set(handles.koniec2,'visible','on');
    
end

function odpdruga_Callback(hObject, eventdata, handles)
global MAX;
global PUNKTY;
global GOOD;
global BAD;
global FS;
global PYTANIE;
global NRPYTANIA;
global BAZAPYTAN;
global DOBREODP;
global ZLEODP;
global LICZNIK;
global ODPOWIEDZI;
global ODP2 ODP3 ODP4 ODP5 ODP6 ODP7 ODP8 ODP9 ODP10;

if NRPYTANIA == 1
    msgbox({'Odpowied� nieprawid�owa' '' 'Dostajesz 0 punkt�w'});
    wavplay(BAD,FS);
    PUNKTY = PUNKTY + 0;
    DOBREODP = DOBREODP + 0;
    ZLEODP = ZLEODP + 1;
    ODPOWIEDZI(NRPYTANIA) = 0;
    set(handles.text3,'String',PUNKTY);
    NRPYTANIA = NRPYTANIA + 1;
    PYTANIE = BAZAPYTAN{NRPYTANIA};
    set(handles.text1,'String',PYTANIE);
    set(handles.odppierwsza,'String',ODP2{1});
    set(handles.odpdruga,'String',ODP2{2});
    set(handles.odptrzecia,'String',ODP2{3});
    LICZNIK = strcat(num2str(NRPYTANIA), ' / ', num2str(MAX));
    set(handles.text5,'String',LICZNIK);

elseif NRPYTANIA == 2
    msgbox({'Odpowied� nieprawid�owa' '' 'Dostajesz 0 punkt�w'});
    wavplay(BAD,FS);
    PUNKTY = PUNKTY + 0;
    DOBREODP = DOBREODP + 0;
    ZLEODP = ZLEODP + 1;
    ODPOWIEDZI(NRPYTANIA) = 0;
    set(handles.text3,'String',PUNKTY);
    NRPYTANIA = NRPYTANIA + 1;
    PYTANIE = BAZAPYTAN{NRPYTANIA};
    set(handles.text1,'String',PYTANIE);
    set(handles.odppierwsza,'String',ODP3{1});
    set(handles.odpdruga,'String',ODP3{2});
    set(handles.odptrzecia,'String',ODP3{3});
    LICZNIK = strcat(num2str(NRPYTANIA), ' / ', num2str(MAX));
    set(handles.text5,'String',LICZNIK);
    
elseif NRPYTANIA == 3
    msgbox({'Odpowied� nieprawid�owa' '' 'Dostajesz 0 punkt�w'});
    wavplay(BAD,FS);
    PUNKTY = PUNKTY + 0;
    DOBREODP = DOBREODP + 0;
    ZLEODP = ZLEODP + 1;
    ODPOWIEDZI(NRPYTANIA) = 0;
    set(handles.text3,'String',PUNKTY);
    NRPYTANIA = NRPYTANIA + 1;
    PYTANIE = BAZAPYTAN{NRPYTANIA};
    set(handles.text1,'String',PYTANIE);
    set(handles.odppierwsza,'String',ODP4{1});
    set(handles.odpdruga,'String',ODP4{2});
    set(handles.odptrzecia,'String',ODP4{3});
    LICZNIK = strcat(num2str(NRPYTANIA), ' / ', num2str(MAX));
    set(handles.text5,'String',LICZNIK);
    
elseif NRPYTANIA == 4
    msgbox({'Odpowied� prawid�owa' '' 'Dostajesz 1 punkt'});
    wavplay(GOOD,FS);
    PUNKTY = PUNKTY + 1;
    DOBREODP = DOBREODP + 1;
    ZLEODP = ZLEODP + 0;
    ODPOWIEDZI(NRPYTANIA) = 1;
    set(handles.text3,'String',PUNKTY);
    NRPYTANIA = NRPYTANIA + 1;
    PYTANIE = BAZAPYTAN{NRPYTANIA};
    set(handles.text1,'String',PYTANIE);
    set(handles.odppierwsza,'String',ODP5{1});
    set(handles.odpdruga,'String',ODP5{2});
    set(handles.odptrzecia,'String',ODP5{3});
    LICZNIK = strcat(num2str(NRPYTANIA), ' / ', num2str(MAX));
    set(handles.text5,'String',LICZNIK);
    
elseif NRPYTANIA == 5
    msgbox({'Odpowied� nieprawid�owa' '' 'Dostajesz 0 punkt�w'});
    wavplay(BAD,FS);
    PUNKTY = PUNKTY + 0;
    DOBREODP = DOBREODP + 0;
    ZLEODP = ZLEODP + 1;
    ODPOWIEDZI(NRPYTANIA) = 0;
    set(handles.text3,'String',PUNKTY);
    NRPYTANIA = NRPYTANIA + 1;
    PYTANIE = BAZAPYTAN{NRPYTANIA};
    set(handles.text1,'String',PYTANIE);
    set(handles.odppierwsza,'String',ODP6{1});
    set(handles.odpdruga,'String',ODP6{2});
    set(handles.odptrzecia,'String',ODP6{3});
    LICZNIK = strcat(num2str(NRPYTANIA), ' / ', num2str(MAX));
    set(handles.text5,'String',LICZNIK);
    
elseif NRPYTANIA == 6
    msgbox({'Odpowied� prawid�owa' '' 'Dostajesz 1 punkt'});
    wavplay(GOOD,FS);
    PUNKTY = PUNKTY + 1;
    DOBREODP = DOBREODP + 1;
    ZLEODP = ZLEODP + 0;
    ODPOWIEDZI(NRPYTANIA) = 1;
    set(handles.text3,'String',PUNKTY);
    NRPYTANIA = NRPYTANIA + 1;
    PYTANIE = BAZAPYTAN{NRPYTANIA};
    set(handles.text1,'String',PYTANIE);
    set(handles.odppierwsza,'String',ODP7{1});
    set(handles.odpdruga,'String',ODP7{2});
    set(handles.odptrzecia,'String',ODP7{3});
    LICZNIK = strcat(num2str(NRPYTANIA), ' / ', num2str(MAX));
    set(handles.text5,'String',LICZNIK);
    
elseif NRPYTANIA == 7
    msgbox({'Odpowied� nieprawid�owa' '' 'Dostajesz 0 punkt�w'});
    wavplay(BAD,FS);
    PUNKTY = PUNKTY + 0;
    DOBREODP = DOBREODP + 0;
    ZLEODP = ZLEODP + 1;
    ODPOWIEDZI(NRPYTANIA) = 0;
    set(handles.text3,'String',PUNKTY);
    NRPYTANIA = NRPYTANIA + 1;
    PYTANIE = BAZAPYTAN{NRPYTANIA};
    set(handles.text1,'String',PYTANIE);
    set(handles.odppierwsza,'String',ODP8{1});
    set(handles.odpdruga,'String',ODP8{2});
    set(handles.odptrzecia,'String',ODP8{3});
    LICZNIK = strcat(num2str(NRPYTANIA), ' / ', num2str(MAX));
    set(handles.text5,'String',LICZNIK);
    
elseif NRPYTANIA == 8
    msgbox({'Odpowied� prawid�owa' '' 'Dostajesz 1 punkt'});
    wavplay(GOOD,FS);
    PUNKTY = PUNKTY + 1;
    DOBREODP = DOBREODP + 1;
    ZLEODP = ZLEODP + 0;
    ODPOWIEDZI(NRPYTANIA) = 1;
    set(handles.text3,'String',PUNKTY);
    NRPYTANIA = NRPYTANIA + 1;
    PYTANIE = BAZAPYTAN{NRPYTANIA};
    set(handles.text1,'String',PYTANIE);
    set(handles.odppierwsza,'String',ODP9{1});
    set(handles.odpdruga,'String',ODP9{2});
    set(handles.odptrzecia,'String',ODP9{3});
    LICZNIK = strcat(num2str(NRPYTANIA), ' / ', num2str(MAX));
    set(handles.text5,'String',LICZNIK);
    
elseif NRPYTANIA == 9
    msgbox({'Odpowied� nieprawid�owa' '' 'Dostajesz 0 punkt�w'});
    wavplay(BAD,FS);
    PUNKTY = PUNKTY + 0;
    DOBREODP = DOBREODP + 0;
    ZLEODP = ZLEODP + 1;
    ODPOWIEDZI(NRPYTANIA) = 0;
    set(handles.text3,'String',PUNKTY);
    NRPYTANIA = NRPYTANIA + 1;
    PYTANIE = BAZAPYTAN{NRPYTANIA};
    set(handles.text1,'String',PYTANIE);
    set(handles.odppierwsza,'String',ODP10{1});
    set(handles.odpdruga,'String',ODP10{2});
    set(handles.odptrzecia,'String',ODP10{3});
    LICZNIK = strcat(num2str(NRPYTANIA), ' / ', num2str(MAX));
    set(handles.text5,'String',LICZNIK);
    
elseif NRPYTANIA == 10
    msgbox({'Odpowied� prawid�owa' '' 'Dostajesz 1 punkt'});
    wavplay(GOOD,FS);
    PUNKTY = PUNKTY + 1;
    DOBREODP = DOBREODP + 1;
    ZLEODP = ZLEODP + 0;
    ODPOWIEDZI(NRPYTANIA) = 1;
    set(handles.text3,'String',PUNKTY);

    set(handles.text1,'visible','off');
    set(handles.text2,'visible','off');
    set(handles.text4,'visible','off');
    set(handles.text3,'visible','off');
    set(handles.text5,'visible','off');
    set(handles.odppierwsza,'visible','off');
    set(handles.odpdruga,'visible','off');
    set(handles.odptrzecia,'visible','off');
    set(handles.uipanel1,'visible','off');

    
    
    axes(handles.wyniki);
    bar(ODPOWIEDZI,'stacked','g');
    
    prawidlowe = strcat('Prawid�owe odpowiedzi:  ', num2str(DOBREODP));
    nieprawidlowe = strcat('Nieprawid�owe odpowiedzi:  ', num2str(ZLEODP));
    procent = ((DOBREODP * 100) / MAX);
    procentprawidlowych = strcat('% prawid�owych:  ', num2str(procent), '%');
    set(handles.podsumowanie,'String',{prawidlowe, nieprawidlowe, procentprawidlowych, '','','', 'Pytania na kt�re odpowiedziano poprawnie'});
    set(handles.wyniki,'visible','on');
    set(handles.podsumowanie,'visible','on');
    
    set(handles.koniec2,'visible','on');

end

function odptrzecia_Callback(hObject, eventdata, handles)
global MAX;
global PUNKTY;
global GOOD;
global BAD;
global FS;
global PYTANIE;
global NRPYTANIA;
global BAZAPYTAN;
global DOBREODP;
global ZLEODP;
global LICZNIK;
global ODPOWIEDZI;
global ODP2 ODP3 ODP4 ODP5 ODP6 ODP7 ODP8 ODP9 ODP10;

if NRPYTANIA == 1
    msgbox({'Odpowied� nieprawid�owa' '' 'Dostajesz 0 punkt�w'});
    wavplay(BAD,FS);
    PUNKTY = PUNKTY + 0;
    DOBREODP = DOBREODP + 0;
    ZLEODP = ZLEODP + 1;
    ODPOWIEDZI(NRPYTANIA) = 0;
    set(handles.text3,'String',PUNKTY);
    NRPYTANIA = NRPYTANIA + 1;
    PYTANIE = BAZAPYTAN{NRPYTANIA};
    set(handles.text1,'String',PYTANIE);
    set(handles.odppierwsza,'String',ODP2{1});
    set(handles.odpdruga,'String',ODP2{2});
    set(handles.odptrzecia,'String',ODP2{3});
    LICZNIK = strcat(num2str(NRPYTANIA), ' / ', num2str(MAX));
    set(handles.text5,'String',LICZNIK);

elseif NRPYTANIA == 2
    msgbox({'Odpowied� prawid�owa' '' 'Dostajesz 1 punkt'});
    wavplay(GOOD,FS);
    PUNKTY = PUNKTY + 1;
    DOBREODP = DOBREODP + 1;
    ZLEODP = ZLEODP + 0;
    ODPOWIEDZI(NRPYTANIA) = 1;
    set(handles.text3,'String',PUNKTY);
    NRPYTANIA = NRPYTANIA + 1;
    PYTANIE = BAZAPYTAN{NRPYTANIA};
    set(handles.text1,'String',PYTANIE);
    set(handles.odppierwsza,'String',ODP3{1});
    set(handles.odpdruga,'String',ODP3{2});
    set(handles.odptrzecia,'String',ODP3{3});
    LICZNIK = strcat(num2str(NRPYTANIA), ' / ', num2str(MAX));
    set(handles.text5,'String',LICZNIK);
    
elseif NRPYTANIA == 3
    msgbox({'Odpowied� prawid�owa' '' 'Dostajesz 1 punkt'});
    wavplay(GOOD,FS);
    PUNKTY = PUNKTY + 1;
    DOBREODP = DOBREODP + 1;
    ZLEODP = ZLEODP + 0;
    ODPOWIEDZI(NRPYTANIA) = 1;
    set(handles.text3,'String',PUNKTY);
    NRPYTANIA = NRPYTANIA + 1;
    PYTANIE = BAZAPYTAN{NRPYTANIA};
    set(handles.text1,'String',PYTANIE);
    set(handles.odppierwsza,'String',ODP4{1});
    set(handles.odpdruga,'String',ODP4{2});
    set(handles.odptrzecia,'String',ODP4{3});
    LICZNIK = strcat(num2str(NRPYTANIA), ' / ', num2str(MAX));
    set(handles.text5,'String',LICZNIK);
    
elseif NRPYTANIA == 4
    msgbox({'Odpowied� nieprawid�owa' '' 'Dostajesz 0 punkt�w'});
    wavplay(BAD,FS);
    PUNKTY = PUNKTY + 0;
    DOBREODP = DOBREODP + 0;
    ZLEODP = ZLEODP + 1;
    ODPOWIEDZI(NRPYTANIA) = 0;
    set(handles.text3,'String',PUNKTY);
    NRPYTANIA = NRPYTANIA + 1;
    PYTANIE = BAZAPYTAN{NRPYTANIA};
    set(handles.text1,'String',PYTANIE);
    set(handles.odppierwsza,'String',ODP5{1});
    set(handles.odpdruga,'String',ODP5{2});
    set(handles.odptrzecia,'String',ODP5{3});
    LICZNIK = strcat(num2str(NRPYTANIA), ' / ', num2str(MAX));
    set(handles.text5,'String',LICZNIK);
    
elseif NRPYTANIA == 5
    msgbox({'Odpowied� nieprawid�owa' '' 'Dostajesz 0 punkt�w'});
    wavplay(BAD,FS);
    PUNKTY = PUNKTY + 0;
    DOBREODP = DOBREODP + 0;
    ZLEODP = ZLEODP + 1;
    ODPOWIEDZI(NRPYTANIA) = 0;
    set(handles.text3,'String',PUNKTY);
    NRPYTANIA = NRPYTANIA + 1;
    PYTANIE = BAZAPYTAN{NRPYTANIA};
    set(handles.text1,'String',PYTANIE);
    set(handles.odppierwsza,'String',ODP6{1});
    set(handles.odpdruga,'String',ODP6{2});
    set(handles.odptrzecia,'String',ODP6{3});
    LICZNIK = strcat(num2str(NRPYTANIA), ' / ', num2str(MAX));
    set(handles.text5,'String',LICZNIK);
    
elseif NRPYTANIA == 6
    msgbox({'Odpowied� nieprawid�owa' '' 'Dostajesz 0 punkt�w'});
    wavplay(BAD,FS);
    PUNKTY = PUNKTY + 0;
    DOBREODP = DOBREODP + 0;
    ZLEODP = ZLEODP + 1;
    ODPOWIEDZI(NRPYTANIA) = 0;
    set(handles.text3,'String',PUNKTY);
    NRPYTANIA = NRPYTANIA + 1;
    PYTANIE = BAZAPYTAN{NRPYTANIA};
    set(handles.text1,'String',PYTANIE);
    set(handles.odppierwsza,'String',ODP7{1});
    set(handles.odpdruga,'String',ODP7{2});
    set(handles.odptrzecia,'String',ODP7{3});
    LICZNIK = strcat(num2str(NRPYTANIA), ' / ', num2str(MAX));
    set(handles.text5,'String',LICZNIK);
    
elseif NRPYTANIA == 7
    msgbox({'Odpowied� prawid�owa' '' 'Dostajesz 1 punkt'});
    wavplay(GOOD,FS);
    PUNKTY = PUNKTY + 1;
    DOBREODP = DOBREODP + 1;
    ZLEODP = ZLEODP + 0;
    ODPOWIEDZI(NRPYTANIA) = 1;
    set(handles.text3,'String',PUNKTY);
    NRPYTANIA = NRPYTANIA + 1;
    PYTANIE = BAZAPYTAN{NRPYTANIA};
    set(handles.text1,'String',PYTANIE);
    set(handles.odppierwsza,'String',ODP8{1});
    set(handles.odpdruga,'String',ODP8{2});
    set(handles.odptrzecia,'String',ODP8{3});
    LICZNIK = strcat(num2str(NRPYTANIA), ' / ', num2str(MAX));
    set(handles.text5,'String',LICZNIK);
    
elseif NRPYTANIA == 8
    msgbox({'Odpowied� nieprawid�owa' '' 'Dostajesz 0 punkt�w'});
    wavplay(BAD,FS);
    PUNKTY = PUNKTY + 0;
    DOBREODP = DOBREODP + 0;
    ZLEODP = ZLEODP + 1;
    ODPOWIEDZI(NRPYTANIA) = 0;
    set(handles.text3,'String',PUNKTY);
    NRPYTANIA = NRPYTANIA + 1;
    PYTANIE = BAZAPYTAN{NRPYTANIA};
    set(handles.text1,'String',PYTANIE);
    set(handles.odppierwsza,'String',ODP9{1});
    set(handles.odpdruga,'String',ODP9{2});
    set(handles.odptrzecia,'String',ODP9{3});
    LICZNIK = strcat(num2str(NRPYTANIA), ' / ', num2str(MAX));
    set(handles.text5,'String',LICZNIK);
    
elseif NRPYTANIA == 9
    msgbox({'Odpowied� nieprawid�owa' '' 'Dostajesz 0 punkt�w'});
    wavplay(BAD,FS);
    PUNKTY = PUNKTY + 0;
    DOBREODP = DOBREODP + 0;
    ZLEODP = ZLEODP + 1;
    ODPOWIEDZI(NRPYTANIA) = 0;
    set(handles.text3,'String',PUNKTY);
    NRPYTANIA = NRPYTANIA + 1;
    PYTANIE = BAZAPYTAN{NRPYTANIA};
    set(handles.text1,'String',PYTANIE);
    set(handles.odppierwsza,'String',ODP10{1});
    set(handles.odpdruga,'String',ODP10{2});
    set(handles.odptrzecia,'String',ODP10{3});
    LICZNIK = strcat(num2str(NRPYTANIA), ' / ', num2str(MAX));
    set(handles.text5,'String',LICZNIK);
    
elseif NRPYTANIA == 10
    msgbox({'Odpowied� nieprawid�owa' '' 'Dostajesz 0 punkt�w'});
    wavplay(BAD,FS);
    PUNKTY = PUNKTY + 0;
    DOBREODP = DOBREODP + 0;
    ZLEODP = ZLEODP + 1;
    ODPOWIEDZI(NRPYTANIA) = 0;
    set(handles.text3,'String',PUNKTY);

    set(handles.text1,'visible','off');
    set(handles.text2,'visible','off');
    set(handles.text4,'visible','off');
    set(handles.text3,'visible','off');
    set(handles.text5,'visible','off');
    set(handles.odppierwsza,'visible','off');
    set(handles.odpdruga,'visible','off');
    set(handles.odptrzecia,'visible','off');
    set(handles.uipanel1,'visible','off');
    
    axes(handles.wyniki);
    bar(ODPOWIEDZI,'stacked','g');
    
    prawidlowe = strcat('Prawid�owe odpowiedzi:  ', num2str(DOBREODP));
    nieprawidlowe = strcat('Nieprawid�owe odpowiedzi:  ', num2str(ZLEODP));
    procent = ((DOBREODP * 100) / MAX);
    procentprawidlowych = strcat('% prawid�owych:  ', num2str(procent), '%');
    set(handles.podsumowanie,'String',{prawidlowe, nieprawidlowe, procentprawidlowych, '','','', 'Pytania na kt�re odpowiedziano poprawnie'});
    set(handles.wyniki,'visible','on');
    set(handles.podsumowanie,'visible','on');
    
    set(handles.koniec2,'visible','on');

end



function oautorze_Callback(hObject, eventdata, handles)
    
    set(handles.nowagra,'visible','off');
    set(handles.oautorze,'visible','off');
    set(handles.koniec,'visible','off');

    set(handles.text1,'visible','off');
    set(handles.text2,'visible','off');
    set(handles.text4,'visible','off');
    set(handles.text3,'visible','off');
    set(handles.text5,'visible','off');
    set(handles.odppierwsza,'visible','off');
    set(handles.odpdruga,'visible','off');
    set(handles.odptrzecia,'visible','off');
    set(handles.uipanel1,'visible','off');
    set(handles.wroc,'visible','on');
    set(handles.infoomnie,'visible','on');


function wroc_Callback(hObject, eventdata, handles)
    set(handles.nowagra,'visible','on');
    set(handles.oautorze,'visible','on');
    set(handles.koniec,'visible','on');
    set(handles.wroc,'visible','off');
    set(handles.infoomnie,'visible','off');




function koniec_Callback(hObject, eventdata, handles)
 close(handles.figure1);

function koniec2_Callback(hObject, eventdata, handles)
 close(handles.figure1);
